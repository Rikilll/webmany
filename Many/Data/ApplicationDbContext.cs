﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Many.Models;
using Many.Models.MyModels;

namespace Many.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Transactions> TransactionsDb { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Transactions>()
               .HasOne(o => o.ApplicationUserSent)
               .WithMany(o => o.TransactionsSent)
               .HasForeignKey(o => o.IdSent);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(p => p.TransactionsSent)
                .WithOne(p => p.ApplicationUserSent)
                .HasPrincipalKey(p => p.Id)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Transactions>()
               .HasOne(o => o.ApplicationUserReceived)
               .WithMany(o => o.TransactionsReceived)
               .HasForeignKey(o => o.IdReceived);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(p => p.TransactionsReceived)
                .WithOne(p => p.ApplicationUserReceived)
                .HasPrincipalKey(p => p.Id)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
