﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Many.Models.MyViewModel
{
    public class CreateTranzactionsViewModel
    {
        public string Number { get; set; }
        public double Money { get; set; }
    }
}
