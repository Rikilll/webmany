﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Many.Models.MyModels
{
    public class Transactions
    {
        public int Id { get; set; }
        public string IdSent { get; set; }
        public ApplicationUser ApplicationUserSent { get; set; }
        public string IdReceived { get; set; }
        public ApplicationUser ApplicationUserReceived { get; set; }

        public DateTime date { get; set; }
        public double Money { get; set; }

    }
}
