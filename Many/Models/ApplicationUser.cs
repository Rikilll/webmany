﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Many.Models.MyModels;
using Microsoft.AspNetCore.Identity;

namespace Many.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public double Balance { get; set; }
        public string Name { get; set; }
        public IEnumerable<Transactions> TransactionsSent { get; set; }
        public IEnumerable<Transactions> TransactionsReceived { get; set; }
    }
}
