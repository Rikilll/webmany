﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Many.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Obligatory_field")]

        public string Number { get; set; }

        [Required(ErrorMessage = "Obligatory_field")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember_me?")]
        public bool RememberMe { get; set; }
    }
}
