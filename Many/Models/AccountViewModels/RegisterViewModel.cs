﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Many.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Obligatory_field")]
        [EmailAddress]
        [Display(Name = "Email.Display")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Obligatory_field")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Obligatory_field")]
        [StringLength(100, ErrorMessage = "PasswordError", MinimumLength = 3)]
        [DataType(DataType.Password)]
        [Display(Name = "Password.Display")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm_password.Dysplay")]
        [Compare("Password", ErrorMessage = "ComparePasswordsError")]
        public string ConfirmPassword { get; set; }
    }
}
