﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Many.Data;
using Many.Models.MyModels;
using Many.Models;
using Microsoft.AspNetCore.Identity;
using Many.Models.MyViewModel;

namespace Many.Controllers
{
    public class TransactionsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public TransactionsController(ApplicationDbContext context,UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Transactions
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            ViewBag.Balance = user.Balance;
            var applicationDbContext = _context.TransactionsDb.Where(p=>p.IdReceived == user.Id || p.IdSent == user.Id).Include(t => t.ApplicationUserReceived).Include(t => t.ApplicationUserSent);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Transactions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transactions = await _context.TransactionsDb
                .Include(t => t.ApplicationUserReceived)
                .Include(t => t.ApplicationUserSent)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (transactions == null)
            {
                return NotFound();
            }

            return View(transactions);
        }

        // GET: Transactions/Create
        public async Task<IActionResult> Create()
        {
            var user = await _userManager.GetUserAsync(User);
            ViewBag.Balance = user.Balance;
            ViewData["IdReceived"] = new SelectList(_context.Users, "Id", "Name");

            return View();
        }

        // POST: Transactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateTranzactionsViewModel model, Transactions transactions)
        {
            var user = await _userManager.GetUserAsync(User);
            ViewBag.Balance = user.Balance;
            var user2 = await _context.Users.FirstOrDefaultAsync(r => r.UserName == model.Number);
            

            if (ModelState.IsValid && user.Balance>model.Money && user2!=null)
            {
                transactions.IdSent = user.Id;
                transactions.IdReceived = user2.Id;
                transactions.date = DateTime.Now;
                transactions.Money = model.Money;
                _context.Add(transactions);
                user.Balance -= model.Money;
                user2.Balance += model.Money;
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdReceived"] = new SelectList(_context.Users, "Id", "Name", transactions.IdReceived);

            return View(model);

        }
        public async Task<IActionResult> Replenish()
        {
            var user = await _userManager.GetUserAsync(User);
            ViewBag.Balance = user.Balance;


            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Replenish(CreateTranzactionsViewModel model, Transactions transactions)
        {
            var user = await _userManager.GetUserAsync(User);
            ViewBag.Balance = user.Balance;

            if (ModelState.IsValid)
            {
                transactions.IdSent = user.Id;
                transactions.IdReceived = user.Id;
                transactions.date = DateTime.Now;
                transactions.Money = model.Money;
                _context.Add(transactions);
                user.Balance += model.Money;
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }


            return View(model);

        }
        public async Task<IActionResult> AnonimReplenish()
        {

            ViewData["IdReceived"] = new SelectList(_context.Users, "Id", "Name");

            return View();
        }

        // POST: Transactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AnonimReplenish(CreateTranzactionsViewModel model, Transactions transactions)
        {

            var user2 = await _context.Users.FirstOrDefaultAsync(r => r.UserName == model.Number);


            if (ModelState.IsValid && user2 != null)
            {
                transactions.IdSent = user2.Id;
                transactions.IdReceived = user2.Id;
                transactions.date = DateTime.Now;
                transactions.Money = model.Money;
                _context.Add(transactions);
                user2.Balance += model.Money;
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
            }
            ViewData["IdReceived"] = new SelectList(_context.Users, "Id", "Name", transactions.IdReceived);

            return View(model);

        }
    }
}
